export default function inCartCount() {
    const cart = JSON.parse(localStorage.getItem('cart'))
    if (cart) {return cart.length}
    return 0
}