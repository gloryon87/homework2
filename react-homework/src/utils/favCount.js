export default function favCount() {
    const fav = JSON.parse(localStorage.getItem('favourites'))
    if (fav) {return fav.length}
    return 0
    
}