import React from 'react'
// import PropTypes from 'prop-types'
import GoodsList from '../../components/goodsList/goodsList'

function Favourites() {
  return (
    <div className='App'>
    <GoodsList
    favOnly={true}
    listName= 'Favourites'
    />
    </div>
  )
}

// Favourites.propTypes = {}

export default Favourites
