import axios from 'axios'

const url = process.env.PUBLIC_URL + 'goods.json'

export default async function getGoodsList () {
    try {const {data} = await axios.get(url);
    return data;
} catch (err) {
    console.error(err);
}
}