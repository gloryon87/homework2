import { TOGGLE_MODAL } from "../actions/modalActions";

export const setModalOpen = (item) => ({type: TOGGLE_MODAL, payload: item})