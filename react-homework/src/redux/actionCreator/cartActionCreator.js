import { ADD_TO_CART, DELETE_FROM_CART, SET_CART, COUNT_CART } from './../actions/cartActions';

export const setCart = (items) => ({type: SET_CART, payload: items})
export const addToCart = (item) => ({type: ADD_TO_CART, payload: item})
export const deleteFromCart = (id) => ({type: DELETE_FROM_CART, payload: id})
export const countCart = () => ({type: COUNT_CART})

export const setCartFn = () => {
    return async (dispatch, getState) => {
        try {
            const data = await JSON.parse(localStorage.getItem('cart'));
            if (data) {
                dispatch(setCart(data));
            }
            else {dispatch(setCart([]))}
            
            // const favourite = getState().favourite;
            // localStorage.setItem('favourite', JSON.stringify(favourite))
        } catch (error) {
            console.log('ERROR', error);
        }
    }
}


export const addToCartFn = (item) => {
    return async (dispatch, getState) => {
        try {
            const cart = await JSON.parse(localStorage.getItem('cart'));
            cart.push(item)
            localStorage.setItem('cart', JSON.stringify(cart))
            dispatch(addToCart(item));

            // const favourite = getState().favourite;
            // localStorage.setItem('favourite', JSON.stringify(favourite))
        } catch (error) {
            console.log('ERROR', error);
        }
    }
}

export const deleteFromCartFn = (id) => {
    return async (dispatch, getState) => {
        try {
            const cart = await JSON.parse(localStorage.getItem('cart'));
            const index = cart.findIndex((elem) => {return elem.article === id})
            cart.splice(index, 1)
            localStorage.setItem('favourites', JSON.stringify(cart))
            dispatch(deleteFromCart(id));

                // const favourite = getState().favourite;
                // localStorage.setItem('favourite', JSON.stringify(favourite))
            
        } catch (error) {
            console.log('ERROR', error);
        }
    }
}