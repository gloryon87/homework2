import { FETCH_GOODS } from './../actions/goodsActions';
import axios from 'axios'

const url = process.env.PUBLIC_URL + 'goods.json'

export const fetchGoods = () => {
    return async (dispatch) => {
        try {
          const {data} = await axios.get(url);
          dispatch({ type: FETCH_GOODS, payload: data });
        } catch (error) {
          console.error(error);
        }
      };
}

