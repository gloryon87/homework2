import { ADD_FAV, DELETE_FAV, SET_FAV, COUNT_FAV } from "../actions/favouriteActions";

export const setFav = (items) => ({type: SET_FAV, payload: items})
export const addFav = (item) => ({type: ADD_FAV, payload: item})
export const deleteFav = (id) => ({type: DELETE_FAV, payload: id})
export const countFav = () => ({type: COUNT_FAV})

export const setFavourites = () => {
    return async (dispatch, getState) => {
        try {
            const data = await JSON.parse(localStorage.getItem('favourites'));
            if (data) {
                dispatch(setFav(data));
            }
            else {dispatch(setFav([]))}
            
            // const favourite = getState().favourite;
            // localStorage.setItem('favourite', JSON.stringify(favourite))
        } catch (error) {
            console.log('ERROR', error);
        }
    }
}


export const addFavourite = (item) => {
    return async (dispatch, getState) => {
        try {
            const favourites = await JSON.parse(localStorage.getItem('favourites'));
            favourites.push(item)
            localStorage.setItem('favourites', JSON.stringify(favourites))
            dispatch(addFav(item));

            // const favourite = getState().favourite;
            // localStorage.setItem('favourite', JSON.stringify(favourite))
        } catch (error) {
            console.log('ERROR', error);
        }
    }
}

export const deleteFavourite = (id) => {
    return async (dispatch, getState) => {
        try {
            const favourites = await JSON.parse(localStorage.getItem('favourites'));
            const index = favourites.findIndex((elem) => {return elem.article === id})
            favourites.splice(index, 1)
            localStorage.setItem('favourites', JSON.stringify(favourites))
            dispatch(deleteFav(id));

                // const favourite = getState().favourite;
                // localStorage.setItem('favourite', JSON.stringify(favourite))
            
        } catch (error) {
            console.log('ERROR', error);
        }
    }
}

