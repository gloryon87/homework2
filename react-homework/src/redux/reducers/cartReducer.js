import { ADD_TO_CART, DELETE_FROM_CART, SET_CART, COUNT_CART } from "../actions/cartActions";
import inCartCount from "../../utils/inCartCount";

const initialValue = {items: []};

const cartReducer = (state = initialValue, action) => {
    switch(action.type) {
        case SET_CART: {
            return { ...state, items: action.payload }
        }

        case ADD_TO_CART: {
            const newItems = [...state.items];
            const item = action.payload;
            newItems.push(item);
            localStorage.setItem('cart', JSON.stringify(newItems));
            return { ...state, items: newItems }

        }
        
        case DELETE_FROM_CART: {
            const newItems = [...state.items];
            const id = action.payload;
            const index = newItems.findIndex(el => el.article === id);
            
            newItems.splice(index, 1);
            
            localStorage.setItem('cart', JSON.stringify(newItems));
            return { ...state, items: newItems }

        }

        case COUNT_CART: {
            const count = inCartCount()
            return {...state, cartCount: count}
        }
        default: return state
    }
}

export default cartReducer