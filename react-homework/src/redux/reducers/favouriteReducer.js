import { ADD_FAV, DELETE_FAV, SET_FAV, COUNT_FAV } from "../actions/favouriteActions";
import favCount from "../../utils/favCount";

const initialValue = {items: []};

const favouriteReducer = (state = initialValue, action) => {
    switch(action.type) {
        case SET_FAV: {
            return { ...state, items: action.payload }
        }

        case ADD_FAV: {
            const newItems = [...state.items];
            const item = action.payload;
            newItems.push(item);
            localStorage.setItem('favourites', JSON.stringify(newItems));
            return { ...state, items: newItems }

        }
        
        case DELETE_FAV: {
            const newItems = [...state.items];
            const id = action.payload;
            const index = newItems.findIndex(el => el.article === id);
            
            newItems.splice(index, 1);
            
            localStorage.setItem('favourites', JSON.stringify(newItems));
            return { ...state, items: newItems }

        }

        case COUNT_FAV: {
            const countFav = favCount()
            return {...state, favCount: countFav}
        }
        default: return state
    }
}

export default favouriteReducer