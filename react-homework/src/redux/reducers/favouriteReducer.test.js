import favouriteReducer from "./favouriteReducer";
import { setFav, addFav, deleteFav } from "../actionCreator/favouriteActionCreator";

const initialValues = {items: []};

describe('Favourite reducer tests', () => {
    test('should favouriteReducer return default value without state and action', () => {
        expect(favouriteReducer(undefined, { type: undefined })).toEqual(initialValues)
    });

    test('should favouriteReducer sets favourites', () => {
        const items = ['test']
        expect(favouriteReducer(initialValues, setFav(items))).toEqual({items: ['test']})
    });

    test('should favouriteReducer adds favourites', () => {
        const item = 'test'
        expect(favouriteReducer(initialValues, addFav(item))).toEqual({items: ['test']})
    });

    test('should favouriteReducer delete from favourites', () => {
        const item = {article: 1}
        expect(favouriteReducer({items: [item]}, deleteFav(item))).toEqual(initialValues)
    });

});