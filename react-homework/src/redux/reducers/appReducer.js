import { combineReducers } from "redux";
import favouriteReducer from "./favouriteReducer";
import goodsReducer from "./goodsReducer";
import cartReducer from "./cartReducer";
import modalReducer from "./modalReducer";

const appReducer = combineReducers({
    favourites: favouriteReducer,
    goods: goodsReducer,
    cart: cartReducer,
    modal: modalReducer
})

export default appReducer
