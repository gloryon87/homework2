import goodsReducer from "./goodsReducer";
// import { fetchGoods } from "../actionCreator/goodsActionCreator";
import { FETCH_GOODS } from "../actions/goodsActions";

const initialValue = [];

describe('Goods reducer tests', () => {
    test('should goodsReducer return default value without state and action', () => {
        expect(goodsReducer(undefined, { type: undefined })).toEqual(initialValue)
    });
    test('should goodsReducer return data', () => {
        const data = [{test: 'test'}]
        const action = { type: FETCH_GOODS, payload: data };
        expect(goodsReducer(initialValue, action)).toEqual(data)
    })

});