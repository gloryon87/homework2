import modalReducer from "./modalReducer";
import { setModalOpen } from "../actionCreator/modalActionCreator";

const initialValues = {item: []}

describe('Modal reducer tests', () => {
    test('should modalReducer return default value without state and action', () => {
        expect(modalReducer(undefined, { type: undefined })).toEqual(initialValues)
    });

    test('should modalReducer change item value', () => {
        const item = ['test']
        expect(modalReducer(undefined, setModalOpen(item))).toEqual({item: ['test']})
    });

});
