import { TOGGLE_MODAL } from "../actions/modalActions";

const initialValue = {item: []};

const modalReducer = (state = initialValue, action) => {
    switch(action.type) {
        case TOGGLE_MODAL: {
            return { item: action.payload }
        }
        default: return state
    }
}

export default modalReducer;