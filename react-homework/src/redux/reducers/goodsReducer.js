import { FETCH_GOODS } from './../actions/goodsActions';

const initialValue = []

const goodsReducer = (state = initialValue, action) => {
    switch(action.type) {
        case FETCH_GOODS: {
            return action.payload.map(good => ({ ...good}))
        }
        default: return state
    }
}

export default goodsReducer