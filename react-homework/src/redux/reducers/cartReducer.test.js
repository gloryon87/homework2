import cartReducer from "./cartReducer";
import { setCart, addToCart, deleteFromCart } from "../actionCreator/cartActionCreator";

const initialValues = {items: []};

describe('Cart reducer tests', () => {
    test('should cartReducer return default value without state and action', () => {
        expect(cartReducer(undefined, { type: undefined })).toEqual(initialValues)
    });

    test('should cartReducer sets cart', () => {
        const items = ['test']
        expect(cartReducer(initialValues, setCart(items))).toEqual({items: ['test']})
    });

    test('should cartReducer adds cart', () => {
        const item = 'test'
        expect(cartReducer(initialValues, addToCart(item))).toEqual({items: ['test']})
    });

    test('should cartReducer delete from cart', () => {
        const item = {article: 1}
        expect(cartReducer({items: [item]}, deleteFromCart(item))).toEqual(initialValues)
    });

});