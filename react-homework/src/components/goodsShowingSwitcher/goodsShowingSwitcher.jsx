import React, { useContext } from 'react'
import { ReactComponent as List } from './list.svg'
import { ReactComponent as ClipboardList } from './clipboard-list.svg'
import styles from './goodsShowingSwitcher.module.scss'
import GoodsShowingContext from '../../context/goodsShowingContext'


function GoodsShowingSwitcher() {
  const {goodsShowing,
    setListGoodsShowing,
    setCardsGoodsShowing} = useContext(GoodsShowingContext)
  
    const list = goodsShowing === 'list';
    const cards = goodsShowing === 'cards';

  return (
    <div className={styles.wrapper}>
    <List onClick={() => {setListGoodsShowing()}} style={list ? {opacity: 0.3} : null}/>
    <ClipboardList onClick={() => {setCardsGoodsShowing()}} height='30px' style={cards ? {opacity: 0.3} : null}/>
    </div>
  )
}

export default GoodsShowingSwitcher