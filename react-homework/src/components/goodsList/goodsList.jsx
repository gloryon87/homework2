import React, { useEffect } from 'react'
import styles from './goodsList.module.scss'
import GoodsCard from '../goodsCard/goodsCard'
import { ReactComponent as Star } from '../goodsCard/star-svgrepo-com.svg'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { useSelector, useDispatch, shallowEqual } from 'react-redux'
import { fetchGoods } from './../../redux/actionCreator/goodsActionCreator';
import { setFavourites, countFav } from '../../redux/actionCreator/favouriteActionCreator'
import { countCart, setCartFn  } from '../../redux/actionCreator/cartActionCreator'
import CheckoutForm from '../checkoutForm/checkoutForm'
import GoodsShowingSwitcher from '../goodsShowingSwitcher/goodsShowingSwitcher'

export default function GoodsList ({listName, favOnly, cart}) {
  const favCount = useSelector((state) => state.favourites.favCount)
  const goods = useSelector((state) => state.goods, shallowEqual)
  const cartCount = useSelector((state) => state.cart.cartCount)
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setCartFn())
    dispatch(countCart())
    dispatch(fetchGoods())
    dispatch(setFavourites())
    dispatch(countFav())
  }, [dispatch])


  return (
    <>
      <header className={styles.header}>
        <h2 className={styles.h2}>{listName}</h2>
        <Link to='/favourites'>
          <Star fill='gold' width='66px' height='66px' />
          <p>{favCount}</p>
        </Link>
          <Link to='/cart'>
          <img
            width='66'
            height='66'
            src='https://img.icons8.com/external-smashingstocks-outline-color-smashing-stocks/66/external-Fruit-Basket-food-and-drinks-smashingstocks-outline-color-smashing-stocks.png'
            alt='external-Fruit-Basket-food-and-drinks-smashingstocks-outline-color-smashing-stocks'
          />
          <p>{cartCount}</p>
        </Link>
      </header>
      {!favOnly && !cart && <GoodsShowingSwitcher/>}
      {cart && <CheckoutForm/>}
      <div className={styles.cards}>
        {goods.map(item => (
          <GoodsCard
            key={item.article}
            item={item}
            favOnly={favOnly}
            cart={cart}
          />
        ))}
      </div>
    </>
  )
}

GoodsList.propTypes = {
  listName: PropTypes.string,
  favOnly: PropTypes.bool,
  cart: PropTypes.bool,
}

GoodsList.defaultProps = {
  listName: 'Goods List',
  favOnly: false,
  cart: false,
}