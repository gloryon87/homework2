import { render, screen, fireEvent } from '@testing-library/react';
import MyButton from './button';

const Component = () => {

    return (
            <MyButton>button</MyButton>
    )
}

test('should MyButton render with snapshot', () => {
    const { asFragment } = render(<Component />);
    expect(asFragment()).toMatchSnapshot();
});

test('should MyButton render', () => { 
    render(<Component />);
    expect(screen.getByRole('button')).toBeInTheDocument();
})

test('should MyButton have children', () => {
    render(<Component />);
    expect(screen.getByText('button')).toBeInTheDocument();
})

test('should MyButton be clicked', () => {
    const mockOnClick = jest.fn()
    render(<MyButton onClick={mockOnClick}></MyButton>);
    fireEvent.click(screen.getByRole('button'));
    expect(mockOnClick).toHaveBeenCalled();
})
