import styles from './button.module.scss'

var classNames = require('classnames');
export default function MyButton ({ btnStyle, onClick, children }) {
    const myClassNames = classNames(styles.btn, btnStyle)
    return(
        <button className={myClassNames} onClick={ onClick }>
            {children}
        </button>
    );
}