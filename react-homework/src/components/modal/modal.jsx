
import styles from './modal.module.scss';
import {ReactComponent as X} from "./x-symbol-svgrepo-com.svg"

export default function MyModal({isOpen, header, closeButton, actions, body, close}) {
    if (!isOpen) return null;
    return (
        <div onClick={() => {close()}} className={styles.overlay}>
            <div onClick={(e) => {e.stopPropagation()}} className={styles.modalContainer}>
                <div className={styles.header}>{header}
                {closeButton && <X width="25px" onClick={() => {close()}} style={{cursor: "pointer"}}/>}</div>
                <div className={styles.body}>{body}</div>
                <div className={styles.actions}>{actions}</div>
            </div>
        </div>
    )
}