import { render, screen } from '@testing-library/react';
import MyModal from "./modal";


const Component = () => {

    return (
            <MyModal isOpen='true' header='header' body='body'/>
    )
}

test('should Modal render', () => {
    const { asFragment } = render(<Component />);
    expect(asFragment()).toMatchSnapshot();
});

test('should Modal have header', () => {
    render(<Component />);
    expect(screen.getByText('header')).toBeInTheDocument();
})

test('should Modal have body', () => {
    render(<Component />);
    expect(screen.getByText('body')).toBeInTheDocument();
})

