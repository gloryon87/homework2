import React from 'react'
import styles from './goodsCard.module.scss'
import MyButton from './../button/button'
import buttonStyles from './../button/button.module.scss'
import { ReactComponent as Star } from './star-svgrepo-com.svg'
import {ReactComponent as X} from './delete-stop-svgrepo-com.svg'
import MyModal from '../modal/modal'
import modalStyles from '../modal/modal.module.scss'
import PropTypes from 'prop-types'
import { useSelector, useDispatch } from 'react-redux'
import {addFav, deleteFav, countFav } from '../../redux/actionCreator/favouriteActionCreator'
import { addToCart, deleteFromCart, countCart } from '../../redux/actionCreator/cartActionCreator'
import { setModalOpen } from '../../redux/actionCreator/modalActionCreator'
import GoodsShowingContext from '../../context/goodsShowingContext'
import { useContext } from 'react'
  
export default function GoodsCard ({ item, favOnly, cart}) {

  const {goodsShowing} = useContext(GoodsShowingContext)
  
    const list = goodsShowing === 'list';

  const favourites = useSelector((state) => state.favourites.items)
  const favouritesIndex = favourites.map((item) => {return item.article})
  let isFav = favouritesIndex.includes(item.article);

  const modalOpen = useSelector((state) => state.modal.item)
  const isModalOpen = modalOpen.article === item.article

  const cartItems = useSelector((state) => state.cart.items)
  const inCart = cartItems.filter((el) => el.article === item.article).length

  function toggleFav() {
    isFav = !isFav
      if (isFav) {
        dispatch(addFav(item))
      } else {
        dispatch(deleteFav(item.article))
      }
      dispatch(countFav())
    };

  const dispatch = useDispatch()

  function onAdd () {
    dispatch(addToCart(item))
    dispatch(countCart())
    dispatch(setModalOpen(false))
  }

  function onDelete() {
    dispatch(deleteFromCart(item.article))
    dispatch(countCart())
    dispatch(setModalOpen(false))
  }

  if (cart) {
    return (
      (inCart > 0) &&
      <div className={styles.wrapper}>
      <X className={styles.x} onClick={() => {
          dispatch(setModalOpen(item))
        }}></X>
      <div className={styles.card} key={item.article}>
      <div className={styles.header}>
        <h2 className={styles.h2}>{item.name}</h2>
        <Star
          className={isFav ? styles.fav : undefined}
          onClick={() => toggleFav()}
        />
      </div>
      <img src={item.url} alt={item.name} width='200px'></img>
      <p>price: {item.price}</p>
      <p>color: {item.color}</p>
      
      <MyModal
        isOpen={isModalOpen}
        header='Delete from cart?'
        closeButton={true}
        close={() => {
          dispatch(setModalOpen(false))
        }}
        body={`Do you want to delete ${item.name}?`}
        actions={
          <>
            <button
              className={modalStyles.button}
              onClick={() => {
                onDelete()
              }}
            >
              Ok
            </button>
            <button
              className={modalStyles.button}
              onClick={() => {
                dispatch(setModalOpen(false))
              }}
            >
              Cancel
            </button>
          </>
        }
      ></MyModal>
    </div>
      <p>quantity: {inCart} </p>
      <p>SUM: {item.price * inCart}</p>
    </div>
    )
  }

  if (list && !favOnly) {
    return (
      <div className={styles.listCard}>
        <Star
          className={isFav ? styles.fav : undefined}
          onClick={() => toggleFav()}
        />
        <img src={item.url} alt={item.name} height='70px'></img>
        <div>
          <h3>{item.name}</h3>
          <p>{item.color}</p>
        </div>
        <p>price: {item.price}</p>
        <MyButton
        btnStyle={buttonStyles.red}
        onClick={() => {
          dispatch(setModalOpen(item))
        }}
      >
        Add to cart
      </MyButton>
      <MyModal
        isOpen={isModalOpen}
        header='Add to cart?'
        closeButton={true}
        close={() => {
          dispatch(setModalOpen(false))
        }}
        body={`Do you want to add ${item.name}?`}
        actions={
          <>
            <button
              className={modalStyles.button}
              onClick={() => {
                onAdd()
              }}
            >
              Ok
            </button>
            <button
              className={modalStyles.button}
              onClick={() => {
                dispatch(setModalOpen(false))
              }}
            >
              Cancel
            </button>
          </>
        }
      ></MyModal>
      </div>
    )
  }

  if (!favOnly || (favOnly && isFav))  {
  return (
    <div className={styles.card} key={item.article}>
      <div className={styles.header}>
        <h2 className={styles.h2}>{item.name}</h2>
        <Star
          className={isFav ? styles.fav : undefined}
          onClick={() => toggleFav()}
        />
      </div>
      <img src={item.url} alt={item.name} width='200px'></img>
      <p>price: {item.price}</p>
      <p>color: {item.color}</p>
      <MyButton
        btnStyle={buttonStyles.red}
        onClick={() => {
          dispatch(setModalOpen(item))
        }}
      >
        Add to cart
      </MyButton>

      <MyModal
        isOpen={isModalOpen}
        header='Add to cart?'
        closeButton={true}
        close={() => {
          dispatch(setModalOpen(false))
        }}
        body={`Do you want to add ${item.name}?`}
        actions={
          <>
            <button
              className={modalStyles.button}
              onClick={() => {
                onAdd()
              }}
            >
              Ok
            </button>
            <button
              className={modalStyles.button}
              onClick={() => {
                dispatch(setModalOpen(false))
              }}
            >
              Cancel
            </button>
          </>
        }
      ></MyModal>
    </div>
  )
}}

GoodsCard.propTypes = {
  item: PropTypes.object,
  favOnly: PropTypes.bool,
  cart: PropTypes.bool
}
