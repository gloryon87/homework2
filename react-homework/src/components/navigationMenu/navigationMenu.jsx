import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './navigationMenu.module.scss'

const NavigationMenu = () => {
    return (
        <header className={styles.header}>
        <NavLink className={styles.link} to='/'>
            Home
        </NavLink>
        <NavLink className={styles.link} to='/favourites'>
            Favourites
        </NavLink>
        <NavLink className={styles.link} to='/cart'>
            Cart
        </NavLink>
        </header>
    );
}

export default NavigationMenu;
