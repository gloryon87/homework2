import React from 'react'
import { Formik, Form, Field } from 'formik'
import { object, string, number } from 'yup'
import styles from './checkoutForm.module.scss'
import { PatternFormat } from 'react-number-format';
import { countCart, setCartFn } from '../../redux/actionCreator/cartActionCreator';
import { useDispatch } from 'react-redux';

const CheckoutForm = () => {
  const validationSchema = object({
    firstName: string().required('First name is required'),
    lastName: string().required('Last name is required'),
    age: number().positive().integer(),
    adress: string().required('Adress is required'),
    mob: string()
      .matches(/^\(\d{3}\) \d{3} \d{2} \d{2}$/, 'Phone number must be exactly 10 digits')
      .required('Phone number is required')
  })

  const initialValues = {
    firstName: '',
    lastName: '',
    age: '',
    adress: '',
    mob: ''
  }
  const dispatch = useDispatch();

  let submit = false;

  const onSumbit = (values, { resetForm }) => {
    console.log(values)
    localStorage.removeItem('cart')
    dispatch(setCartFn())
    dispatch(countCart())
    resetForm()
    submit = true
  }

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSumbit}
      validationSchema={validationSchema}
    >
      {({ isValid, touched, errors }) => {
        return (
          <>
          <Form>
            <h3>Checkout form</h3>

            <div className={styles.wrapper}>
              <label htmlFor='firstName'>First Name*</label>
              <Field type='text' name='firstName' placeholder='First Name' />
              {touched.firstName && errors.firstName && (
                <div className={styles.error}>{errors.firstName}</div>
              )}
              <label htmlFor='lastName'>Last Name*</label>
              <Field type='text' name='lastName' placeholder='Last Name' />
              {touched.lastName && errors.lastName && (
                <div className={styles.error}>{errors.lastName}</div>
              )}
              <label htmlFor='age'>Age</label>
              <Field type='text' name='age' placeholder='Age' />
              <label htmlFor='adress'>Adress*</label>
              <Field type='text' name='adress' placeholder='Adress' />
              {touched.adress && errors.adress && (
                <div className={styles.error}>{errors.adress}</div>
              )}
              <label htmlFor='mob'>Mobile number*</label>
              <Field type='text' name='mob'> 
              {({ field }) => (
            <PatternFormat
              {...field}
              format="(###) ### ## ##"
              mask="_"
              placeholder="Mobile number"
            />
          )}</Field>
              {touched.mob && errors.mob && (
                <div className={styles.error}>{errors.mob}</div>
              )}

              <button disabled={!isValid} type='submit' className={styles.submit}>
                Checkout
              </button>
            </div>
          </Form>
          {submit && <h2>Sucsess!</h2>}
          </>
        )
      }}
    </Formik>
  )
}

export default CheckoutForm
