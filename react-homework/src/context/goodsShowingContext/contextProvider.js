import React, { useState } from 'react';
import GoodsShowingContext from '.'

export default function GoodsShowingContextProvider ({ children }) {
    const [goodsShowing, setGoodsShoeing] = useState('cards');

    const setListGoodsShowing = () => {
        setGoodsShoeing('list')
    }

    const setCardsGoodsShowing = () => {
        setGoodsShoeing('cards')
    }

    const value = {
        goodsShowing,
        setListGoodsShowing,
        setCardsGoodsShowing
    }

    return (
        <GoodsShowingContext.Provider value={value}>
            {children}
        </GoodsShowingContext.Provider>
    )

}

