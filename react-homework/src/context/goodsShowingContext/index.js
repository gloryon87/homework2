import { createContext } from "react";

const GoodsShowingContext = createContext(null);

export default GoodsShowingContext;
