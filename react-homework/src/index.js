import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.scss'
import HomePage from './pages/HomePage/HomePage'
import Cart from './pages/Cart/Cart'
import Favourites from './pages/Favourites/Favourites'
import reportWebVitals from './reportWebVitals'
import NavigationMenu from './components/navigationMenu/navigationMenu'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { Provider } from 'react-redux'
import store from './redux/store'
import GoodsShowingContextProvider from './context/goodsShowingContext/contextProvider'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <GoodsShowingContextProvider>
        <BrowserRouter>
          <NavigationMenu></NavigationMenu>
          <Routes>
            <Route path='/' element=<HomePage /> />
            <Route path='/cart' element=<Cart /> />
            <Route path='/favourites' element=<Favourites /> />
          </Routes>
        </BrowserRouter>
      </GoodsShowingContextProvider>
    </Provider>
  </React.StrictMode>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
